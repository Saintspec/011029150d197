Change 1: added a change in the first paragraph of about.html
Change 2: changed the code for the contact.html file
Change 3: changes the code for the customers.html file

For the merge conflict, I added the git version to the readme branch. To resolve the merge conflict, I made sure the readme branch and the main branch both matched and then I merged the readme branch with the main branch.